const { flatten } = require('./utils/flatten');

exports.onCreatePage = async ({ page, actions, }, pluginOptions) => {
  const { createPage, deletePage } = actions;
  const {
    path = '.',
    languages = ['en'],
    defaultLanguage = 'en',
    redirect = false,
    staticPages = ['/', '/404.html']
  } = pluginOptions;

  const oldPage = Object.assign({}, page);

  if (page.context.language) {
    // Gestion des pages générées
    page.context = {
      ...page.context,
      i18n: {
        ...page.context.i18n,
        language: page.context.language,
        languages,
        messages: flatten(require(`${path}/${page.context.language}.json`)),
        routed: true, // S'il y a une locale, on est déjà en routed.
        originalPath: page.slug, // Remonté par createPage
        // paths, // Remonté par createPage
        redirect,
        defaultLanguage
      }
    };

    deletePage(oldPage);
    createPage(page);
  } else {
    // Gestion des pages statiques
    // > Générer une page par langue de la page statique
    // > Supprimer la page statique
    // > sauf index / 404 ?
    
    page.context = {
      ...page.context,
      language: defaultLanguage,
      i18n: {
        ...page.context.i18n,
        language: defaultLanguage,
        languages,
        messages: flatten(require(`${path}/${defaultLanguage}.json`)),
        routed: false, // page de base React
        originalPath: page.path,
        paths: languages.map(l => ({ label: l, link: page.path })),
        redirect,
        defaultLanguage
      }
    };

    languages.forEach(l => {
      const newPage = Object.assign({}, page);
      newPage.path = `${l}${page.path}`;
      
      newPage.context = {
        ...newPage.context,
        language: l,
        i18n: {
          ...newPage.context.i18n,
          language: l,
          messages: flatten(require(`${path}/${l}.json`)),
          routed: true,
          paths: languages.map(l => ({ label: l, link: page.path })),
        }
      }        
      createPage(newPage);
    }); 
    
    deletePage(oldPage);

    if (staticPages.includes(page.path)) {
      createPage(page);
    }
  }
};