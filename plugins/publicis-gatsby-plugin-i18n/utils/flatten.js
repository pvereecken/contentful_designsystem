exports.flatten = function flatten(nestedOject, prefix = '') {
  return Object.keys(nestedOject).reduce((flattenObjects, key) => {
    let value = nestedOject[key];
    let prefixedKey = prefix ? `${prefix}.${key}` : key;

    if (typeof value === 'string') {
      flattenObjects[prefixedKey] = value;
    } else {
      Object.assign(flattenObjects, flatten(value, prefixedKey));
    }

    return flattenObjects;
  }, {});
}