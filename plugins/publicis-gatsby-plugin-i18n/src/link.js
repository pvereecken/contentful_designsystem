import React from 'react';
import PropTypes from 'prop-types';
import { Link as GatsbyLink, navigate as gatsbyNavigate } from 'gatsby';
import { I18nContextConsumer } from './i18n-context';
import { setLanguage } from './switch';

export const Link = ( { to, language, children, onClick, ...rest }) => {
    return (
        <I18nContextConsumer>
            {i18n => {
                const languageLink = language || i18n.language;
                const link = i18n.routed || language ? `/${languageLink}${to}` : `${to}`;

                const handleClick = e => {
                    if(language) {
                        setLanguage(language);
                    }
                    if(onClick){
                        onClick(e);
                    }
                }

                return (
                    <GatsbyLink {...rest} to={link}>{children}</GatsbyLink>                    
                )
            }}
        </I18nContextConsumer>
    );
}

Link.propTypes = {
  children: PropTypes.node.isRequired,
  to: PropTypes.string,
  language: PropTypes.string,
}

Link.defaultProps = {
  to: '',
}

export default Link;