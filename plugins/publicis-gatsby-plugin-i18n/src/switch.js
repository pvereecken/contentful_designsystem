export const setLanguage = (language) => {
  window.localStorage.setItem('gatsby-intl-language', language);
}

export const getLanguage = () => {
  return window.localStorage.getItem('gatsby-intl-language');
}