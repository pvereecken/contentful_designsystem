import React from 'react';
import { withPrefix } from 'gatsby';
import { IntlProvider } from 'react-intl';

import { I18nContextProvider } from './i18n-context';
import { setLanguage, getLanguage } from './switch';

const withI18nProvider = (i18n) => children => {
  return (
    <IntlProvider locale={i18n.language} defaultLocale={i18n.defaultLanguage} messages={i18n.messages}>
      <I18nContextProvider value={i18n}>{children}</I18nContextProvider>
    </IntlProvider>
  );
};

export default ({ element, props }, pluginOptions) => {
  if (!props) return;

  const { pageContext, location } = props;
  const { defaultLanguage } = pluginOptions;
  const { i18n } = pageContext;
  const { language, languages, redirect, routed, paths } = i18n;

  if (typeof window !== 'undefined') {
    window.__gatsbyI18n = i18n;
  }

  const isRedirect = redirect && !routed;

  if (isRedirect) {
    const { search } = location;

    if (typeof window !== 'undefined') {
      let detected = getLanguage() || defaultLanguage;
      if (!languages.includes(detected)) {
        detected = language;
      }

      const queryParams = search || '';
      const slug = paths?.find(p => p.label === detected).link || '';
      const newUrl = withPrefix(`/${detected}${slug}${queryParams}`);
      setLanguage(detected);

      window.location.replace(newUrl);
    }
  }

  return withI18nProvider(i18n)(element);
};