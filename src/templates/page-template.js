import React from 'react';
import { graphql } from 'gatsby';

import CustomBlocks from '../components/CustomBlocks';
import Layout from '../layout/Layout';
import SEO from '../components/SEO';

const PageTemplate = ({ pageContext, data }) => {
  const locale = pageContext.language;

  const customBlocks = data.contentfulLandingPage.customBlocks;

  return (
    <Layout customClass="page">
      <SEO lang={locale} />

      <CustomBlocks data={customBlocks} />
    </Layout>
  );
};

export const query = graphql`
  query pageBySlug($slug: String!, $language: String!) {
    contentfulLandingPage(slug: { eq: $slug }, node_locale: { eq: $language }) {
      customBlocks {
        __typename
        ... on Node {
          ... on ContentfulLayout {
            id
            themeLayout
            backgroundColor
            columns {
              backgroundColor
              columnWidth
              verticalAlignement
              horizontalAlignement
              customBlocks {
                __typename
                ... on Node {
                  ... on ContentfulGallery {
                    id
                    title
                    titleLevel
                    textColor
                    backgroundColor
                    horizontalAlignement
                    imagesPerRow
                    coverImage
                    medias {
                      gatsbyImageData(layout: CONSTRAINED, formats: WEBP)
                      description
                    }
                  }
                  ... on ContentfulSpace {
                    id
                    backgroundColor
                    height
                    units
                  }
                  ... on ContentfulLink {
                    linkColor
                    linkSize
                    linkType
                    linkText: text
                    url
                    targetBlank
                  }
                  ... on ContentfulTitle {
                    title
                    titleLevel
                    themeLayout
                    textColor
                    horizontalAlignement
                    backgroundColor
                  }
                  ... on ContentfulTextContent {
                    themeLayout
                    horizontalAlignement
                    backgroundColor
                    textColor
                    contentText {
                      childMarkdownRemark {
                        html
                      }
                    }
                  }
                  ... on ContentfulPictoTitleText {
                    title
                    titleLevel
                    themeLayout
                    pictoPosition
                    picto {
                      gatsbyImageData(width: 50, layout: FIXED, formats: WEBP)
                      description
                    }
                    horizontalAlignement
                    backgroundColor
                    textColor
                    contentText: text {
                      childMarkdownRemark {
                        html
                      }
                    }
                  }
                }
              }
            }
          }

          ... on ContentfulHeroBanner {
            id
            heroSubtitle
            heroTitle
            displayContentText
            textColor
            backgroundColor
            heroSize
            horizontalAlignement
            parallaxBackgroundImage
            backgroundMedia {
              file {
                url
                contentType
              }
            }
            link
            linkText
            linkType
            linkColor
            linkSize
            targetBlank
          }
          ... on ContentfulGallery {
            id
            title
            titleLevel
            textColor
            themeLayout
            backgroundColor
            horizontalAlignement
            imagesPerRow
            coverImage
            medias {
              gatsbyImageData(layout: CONSTRAINED, formats: WEBP)
              description
            }
          }
          ... on ContentfulSpace {
            id
            backgroundColor
            height
            units
          }
          ... on ContentfulTitle {
            title
            titleLevel
            themeLayout
            textColor
            horizontalAlignement
            backgroundColor
          }
          ... on ContentfulTextContent {
            themeLayout
            horizontalAlignement
            backgroundColor
            textColor
            contentText {
              childMarkdownRemark {
                html
              }
            }
          }
          ... on ContentfulPictoTitleText {
            title
            titleLevel
            themeLayout
            pictoPosition
            picto {
              gatsbyImageData(width: 30, layout: FIXED)
              description
            }
            horizontalAlignement
            backgroundColor
            textColor
            contentText: text {
              childMarkdownRemark {
                html
              }
            }
          }
        }
      }
    }
  }
`;

export default PageTemplate;
