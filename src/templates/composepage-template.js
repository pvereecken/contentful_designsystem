import React from 'react';
import { graphql } from 'gatsby';

import CustomBlocks from '../components/CustomBlocks';
import Layout from '../layout/Layout';
import PeopleTemplate from '../components/people-template';
import SEO from '../components/SEO';

const ComposepageTemplate = ({ pageContext, data }) => {
  const locale = pageContext.language;

  const pageData = data.contentfulComposePage.content;

  return (
    <Layout customClass="page">
      <SEO lang={locale} />
      {pageData.__typename === 'ContentfulPeople' && (
        <PeopleTemplate data={pageData} />
      )}
      {pageData.__typename === 'ContentfulLandingPage' && (
        <CustomBlocks data={pageData.customBlocks} />
      )}
    </Layout>
  );
};

export const query = graphql`
  query composepageBySlug($slug: String!, $language: String!) {
    contentfulComposePage(slug: { eq: $slug }, node_locale: { eq: $language }) {
      content {
        __typename
        ... on Node {
          ... on ContentfulPeople {
            id
            name
            biography {
              childMarkdownRemark {
                html
              }
            }
            firstName
            lastName
            picture {
              gatsbyImageData(layout: FIXED, width: 200)
            }
          }
          ... on ContentfulLandingPage {
            customBlocks {
              __typename
              ... on Node {
                ... on ContentfulLayout {
                  id
                  themeLayout
                  backgroundColor
                  columns {
                    backgroundColor
                    columnWidth
                    verticalAlignement
                    horizontalAlignement
                    customBlocks {
                      __typename
                      ... on Node {
                        ... on ContentfulGallery {
                          id
                          title
                          titleLevel
                          textColor
                          backgroundColor
                          horizontalAlignement
                          imagesPerRow
                          coverImage
                          medias {
                            gatsbyImageData(layout: CONSTRAINED, formats: WEBP)
                            description
                          }
                        }
                        ... on ContentfulSpace {
                          id
                          backgroundColor
                          height
                          units
                        }
                        ... on ContentfulLink {
                          linkColor
                          linkSize
                          linkType
                          linkText: text
                          url
                          targetBlank
                        }
                        ... on ContentfulTitle {
                          title
                          titleLevel
                          themeLayout
                          textColor
                          horizontalAlignement
                          backgroundColor
                        }
                        ... on ContentfulTextContent {
                          themeLayout
                          horizontalAlignement
                          backgroundColor
                          textColor
                          contentText {
                            childMarkdownRemark {
                              html
                            }
                          }
                        }
                        ... on ContentfulPictoTitleText {
                          title
                          titleLevel
                          themeLayout
                          pictoPosition
                          picto {
                            gatsbyImageData(
                              width: 50
                              layout: FIXED
                              formats: WEBP
                            )
                            description
                          }
                          horizontalAlignement
                          backgroundColor
                          textColor
                          contentText: text {
                            childMarkdownRemark {
                              html
                            }
                          }
                        }
                      }
                    }
                  }
                }

                ... on ContentfulHeroBanner {
                  id
                  heroSubtitle
                  heroTitle
                  displayContentText
                  textColor
                  backgroundColor
                  heroSize
                  horizontalAlignement
                  parallaxBackgroundImage
                  backgroundMedia {
                    file {
                      url
                      contentType
                    }
                  }
                  link
                  linkText
                  linkType
                  linkColor
                  linkSize
                  targetBlank
                }
                ... on ContentfulGallery {
                  id
                  title
                  titleLevel
                  textColor
                  themeLayout
                  backgroundColor
                  horizontalAlignement
                  imagesPerRow
                  coverImage
                  medias {
                    gatsbyImageData(layout: CONSTRAINED, formats: WEBP)
                    description
                  }
                }
                ... on ContentfulSpace {
                  id
                  backgroundColor
                  height
                  units
                }
                ... on ContentfulTitle {
                  title
                  titleLevel
                  themeLayout
                  textColor
                  horizontalAlignement
                  backgroundColor
                }
                ... on ContentfulTextContent {
                  themeLayout
                  horizontalAlignement
                  backgroundColor
                  textColor
                  contentText {
                    childMarkdownRemark {
                      html
                    }
                  }
                }
                ... on ContentfulPictoTitleText {
                  title
                  titleLevel
                  themeLayout
                  pictoPosition
                  picto {
                    gatsbyImageData(width: 30, layout: FIXED)
                    description
                  }
                  horizontalAlignement
                  backgroundColor
                  textColor
                  contentText: text {
                    childMarkdownRemark {
                      html
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

export default ComposepageTemplate;
