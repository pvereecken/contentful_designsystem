import React from 'react';
import styled, { props } from 'styled-components';

import { initBkgColor } from '../../utils/helpers/colorHelpers';

const Space = ({ data }) => {
  var classNames = require('classnames');

  const { backgroundColor, height, units } = data;

  return (
    <SpaceWrapper
      className={initBkgColor(backgroundColor)}
      height={height}
      units={units}
    />
  );
};

const SpaceWrapper = styled.div`
  height: ${props => props.height + props.units};
`;

export default Space;
