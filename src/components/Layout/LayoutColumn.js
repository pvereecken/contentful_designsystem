import React from 'react';
import { initColumnsSize } from '../../utils/helpers/sizeHelpers';
import { initBkgColor } from '../../utils/helpers/colorHelpers';
import {
  initVerticalAlign,
  initHorizontalAlign,
} from '../../utils/helpers/alignementHelpers';

import CustomBlocks from '../CustomBlocks';

const LayoutColumn = ({ column }) => {
  var classNames = require('classnames');

  const {
    backgroundColor,
    columnWidth,
    verticalAlignement,
    horizontalAlignement,
    customBlocks,
  } = column;

  return (
    <div
      className={classNames(
        'column',
        initColumnsSize(columnWidth),
        initBkgColor(backgroundColor),
        initVerticalAlign(verticalAlignement),
        initHorizontalAlign(horizontalAlignement)
      )}
    >
      <CustomBlocks data={customBlocks} />
    </div>
  );
};

export default LayoutColumn;
