import React from 'react';
import { initBkgColor } from '../../utils/helpers/colorHelpers';
import { initThemeLayout } from '../../utils/helpers/themeLayoutHelpers';

import LayoutColumn from './LayoutColumn';

import '../../assets/styles/layout/_columns.scss';

const LayoutWrapper = ({ data }) => {
  var classNames = require('classnames');

  const { backgroundColor, themeLayout, columns } = data;
  return (
    <section className={classNames(initBkgColor(backgroundColor))}>
      <div
        className={classNames(
          'columns is-align-content-stretch',
          initThemeLayout(themeLayout)
        )}
      >
        {columns?.map((column, index) => (
          <LayoutColumn key={index} column={column} />
        ))}
      </div>
    </section>
  );
};

export default LayoutWrapper;
