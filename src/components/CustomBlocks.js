import React from 'react';

import LayoutWrapper from './Layout/LayoutWrapper';
import Hero from './Hero/Hero';
import Gallery from './Medias/Gallery';
import LinkItems from './Link/LinkItems';
import PictoTitleTextLink from './Text/PictoTitleTextLink';
import Space from './Layout/Space';
import Title from './Text/Title';
import Text from './Text/Text';

const CustomBlock = ({ data }) => {
  return (
    <>
      {data?.map((block, index) => {
        switch (block.__typename) {
          case 'ContentfulLayout':
            return <LayoutWrapper key={index} data={block} />;

          case 'ContentfulHeroBanner':
            return <Hero key={index} data={block} />;

          case 'ContentfulGallery':
            return <Gallery key={index} data={block} />;

          case 'ContentfulSpace':
            return <Space key={index} data={block} />;

          case 'ContentfulLink':
            return <LinkItems key={index} data={block} />;

          case 'ContentfulTitle':
            return <Title key={index} data={block} />;

          case 'ContentfulTextContent':
            return <Text key={index} data={block} />;

          case 'ContentfulPictoTitleText':
            return <PictoTitleTextLink key={index} data={block} />;

          default:
            break;
        }
      })}
    </>
  );
};

export default CustomBlock;
