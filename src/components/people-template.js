import React from 'react';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';

const PeopleTemplate = ({ data }) => {
  const { firstName, lastName, picture, biography } = data;

  const imagePath = getImage(picture);

  console.log(data);
  return (
    <div className="container my-4">
      <article className="media">
        <figure className="media-left">
          <GatsbyImage
            image={imagePath}
            alt={picture.description}
            as="div"
            className="image is-square"
          />
        </figure>
        <div className="media-content">
          <div className="content">
            <p>
              <strong>
                {firstName} {lastName}
              </strong>
            </p>
            <div
              dangerouslySetInnerHTML={{
                __html: biography?.childMarkdownRemark.html,
              }}
            />
          </div>
        </div>
      </article>
    </div>
  );
};

export default PeopleTemplate;
