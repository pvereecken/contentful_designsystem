import React, { Fragment } from 'react';

import { Link } from '../../plugins/publicis-gatsby-plugin-i18n';

const Menu = ({ menuItems }) => {
  return (
    <>
      {menuItems?.map((itemMenu, index) => {
        switch (itemMenu.__typename) {
          case 'ContentfulLandingPage':
          case 'ContentfulComposePage':
            return (
              <Link
                key={index}
                to={`/${itemMenu.slug}`}
                className="navbar-item"
                activeClassName="navbar-item--active"
              >
                {itemMenu.title}
              </Link>
            );

          case 'ContentfulLink':
            return (
              <Fragment key={index}>
                {itemMenu.url.startsWith('http') && (
                  <a
                    href={itemMenu.url}
                    rel="noreferrer"
                    target={`${itemMenu.targetBlank ? '_blank' : '_self'}`}
                  >
                    {itemMenu.menuText}
                  </a>
                )}
                {!itemMenu.url.startsWith('http') && (
                  <Link to={itemMenu.url}>{itemMenu.menuText}</Link>
                )}
              </Fragment>
            );

          default:
            break;
        }
      })}
    </>
  );
};

export default Menu;
