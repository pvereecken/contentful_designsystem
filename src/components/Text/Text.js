import React from 'react';

import { initThemeLayout } from '../../utils/helpers/themeLayoutHelpers';
import { initHorizontalAlign } from '../../utils/helpers/alignementHelpers';
import { initBkgColor, initColor } from '../../utils/helpers/colorHelpers';

const Text = ({ data }) => {
  var classNames = require('classnames');

  const {
    textColor,
    backgroundColor,
    themeLayout,
    horizontalAlignement,
    contentText,
  } = data;

  return (
    <div
      className={classNames(
        initThemeLayout(themeLayout),
        initHorizontalAlign(horizontalAlignement),
        initBkgColor(backgroundColor),
        initColor(textColor)
      )}
      dangerouslySetInnerHTML={{
        __html: contentText?.childMarkdownRemark.html,
      }}
    />
  );
};

export default Text;
