import React from 'react';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';

import Title from './Title';
import Text from './Text';

const PictoTitleTextLink = ({ data }) => {
  var classNames = require('classnames');

  const { pictoPosition, picto } = data;

  const imagePath = getImage(picto);

  return (
    <>
      {picto && (pictoPosition === 'Left' || pictoPosition === 'Above') && (
        <div
          className={classNames('columns is-align-items-center', {
            'is-flex-direction-row-reverse': pictoPosition === 'Right',
            'is-flex-direction-column': pictoPosition === 'Above',
          })}
        >
          <GatsbyImage
            image={imagePath}
            alt={picto.description}
            as="div"
            className="column has-picto"
          />
          <div className="column">
            <Title data={data} />
          </div>
        </div>
      )}
      {!picto && <Title data={data} />}

      <Text data={data} />
    </>
  );
};

export default PictoTitleTextLink;
