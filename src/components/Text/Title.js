import React from 'react';

import ReactHtmlParser from 'react-html-parser';

import { initThemeLayout } from '../../utils/helpers/themeLayoutHelpers';
import { initHorizontalAlign } from '../../utils/helpers/alignementHelpers';
import { initTitleLevel } from '../../utils/helpers/titleLevelHelpers';
import { initBkgColor, initColor } from '../../utils/helpers/colorHelpers';

const Title = ({ data }) => {
  var classNames = require('classnames');

  const {
    title,
    titleLevel,
    themeLayout,
    backgroundColor,
    textColor,
    horizontalAlignement,
  } = data;

  return (
    <>
      {ReactHtmlParser(
        initTitleLevel(
          titleLevel,
          title,
          initThemeLayout(themeLayout),
          initHorizontalAlign(horizontalAlignement),
          initBkgColor(backgroundColor),
          initColor(textColor)
        )
      )}
    </>
  );
};

export default Title;
