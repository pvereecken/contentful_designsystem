import React from 'react';

import { Link } from '../../../plugins/publicis-gatsby-plugin-i18n';
import { initColor, initBkgColor } from '../../utils/helpers/colorHelpers';
import { initSize } from '../../utils/helpers/sizeHelpers';

const LinkItems = ({ data }) => {
  var classNames = require('classnames');
  const { url, linkText, linkType, linkColor, linkSize, targetBlank } = data;

  let classType, classColor;
  switch (linkType) {
    case 'Text':
      classType = 'is-text';
      classColor = initColor(linkColor);
      break;

    case 'Button':
    default:
      classType = 'button';
      classColor = initBkgColor(linkColor);
      break;
  }
  return (
    <div>
      {url.startsWith('http') && (
        <a
          href={url}
          rel="noreferrer"
          target={targetBlank ? '_blank' : '_self'}
          className={classNames(classColor, classType, initSize(linkSize))}
        >
          {linkText}
        </a>
      )}
      {!url.startsWith('http') && (
        <Link
          to={url}
          className={classNames(classColor, classType, initSize(linkSize))}
        >
          {linkText}
        </Link>
      )}
    </div>
  );
};

export default LinkItems;
