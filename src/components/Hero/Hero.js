import React from 'react';
import { initBkgColor, initColor } from '../../utils/helpers/colorHelpers';
import { initHorizontalAlign } from '../../utils/helpers/alignementHelpers';

import LinkItems from '../Link/LinkItems';

import '../../assets/styles/layout/hero.scss';

const Hero = ({ data }) => {
  var classNames = require('classnames');

  const {
    heroSubtitle,
    heroTitle,
    displayContentText,
    backgroundColor,
    textColor,
    heroSize,
    horizontalAlignement,
    backgroundMedia,
    parallaxBackgroundImage,
    link,
    linkText,
    linkType,
    linkColor,
    linkSize,
  } = data;

  let classSize;
  switch (heroSize) {
    case 'Small':
      classSize = 'is-small';
      break;

    default:
    case 'Medium':
      classSize = 'is-medium';
      break;

    case 'Large':
      classSize = 'is-large';
      break;

    case 'Half height':
      classSize = 'is-halfheight';
      break;

    case 'Full height':
      classSize = 'is-fullheight';
      break;
  }

  const dataLink = link && {
    url: link,
    linkText: linkText,
    linkType: linkType,
    linkColor: linkColor,
    linkSize: linkSize,
  };

  return (
    <section
      className={classNames(
        'hero',
        initBkgColor(backgroundColor),
        classSize,
        {
          'has-background': backgroundMedia,
        },
        { 'is-parallax': parallaxBackgroundImage }
      )}
      style={
        backgroundMedia?.file?.contentType === 'image/jpeg'
          ? { backgroundImage: `url(${backgroundMedia.file.url})` }
          : {}
      }
    >
      {backgroundMedia?.file?.contentType === 'video/mp4' && (
        <div className="hero-video is-transparent">
          <video muted autoPlay loop>
            <source src={backgroundMedia.file.url} type="video/mp4" />
          </video>
        </div>
      )}

      <div
        className={classNames(
          'hero-body',
          initHorizontalAlign(horizontalAlignement),
          initColor(textColor)
        )}
      >
        {displayContentText && (
          <div>
            {heroTitle && <p className="title">{heroTitle}</p>}
            {heroSubtitle && <p className="subtitle">{heroSubtitle}</p>}
            {link && <LinkItems data={dataLink} />}
          </div>
        )}
      </div>
    </section>
  );
};

export default Hero;
