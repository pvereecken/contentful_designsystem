import React, { useContext } from 'react';

import {
  I18nContext,
  Link,
} from '../../../plugins/publicis-gatsby-plugin-i18n';

const Languages = () => {
  const i18n = useContext(I18nContext);

  return (
    <div className="languages_component navbar-item has-dropdown is-hoverable">
      <a className="navbar-link current_nav">{i18n.language.toUpperCase()}</a>

      <div className="navbar-dropdown select_lang is-hoverable">
        {i18n.paths.map(({ label, link }, i) => (
          <Link
            key={i}
            className={`navbar-item ${label === i18n.language ? 'active' : ''}`}
            to={link}
            language={label}
          >
            {label}
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Languages;
