import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';

import { initColumnsSize } from '../../utils/helpers/sizeHelpers';
import { initThemeLayout } from '../../utils/helpers/themeLayoutHelpers';
import { initHorizontalAlign } from '../../utils/helpers/alignementHelpers';
import { initTitleLevel } from '../../utils/helpers/titleLevelHelpers';
import { initBkgColor, initColor } from '../../utils/helpers/colorHelpers';

const Gallery = ({ data }) => {
  var classNames = require('classnames');

  const {
    title,
    titleLevel,
    textColor,
    backgroundColor,
    themeLayout,
    horizontalAlignement,
    imagesPerRow,
    coverImage,
    medias,
  } = data;

  return (
    <section className={classNames(initBkgColor(backgroundColor))}>
      {ReactHtmlParser(
        initTitleLevel(
          titleLevel,
          title,
          initThemeLayout(themeLayout),
          false,
          initBkgColor(backgroundColor),
          initColor(textColor)
        )
      )}
      <div
        className={classNames(
          'columns is-flex-wrap-wrap',
          initThemeLayout(themeLayout),
          initHorizontalAlign(horizontalAlignement)
        )}
      >
        {medias?.map((media, index) => {
          const imagePath = getImage(media);

          return (
            <GatsbyImage
              key={index}
              image={imagePath}
              alt={media.description}
              as="figure"
              className={classNames(
                'column',
                initColumnsSize(`img-${imagesPerRow}`)
              )}
              objectFit={coverImage ? 'cover' : 'contain'}
            />
          );
        })}
      </div>
    </section>
  );
};

export default Gallery;
