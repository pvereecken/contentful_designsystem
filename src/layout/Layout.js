import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import { Helmet } from 'react-helmet';

import Navbar from './Navbar';
import Footer from './Footer';

const getPrintCss = graphql`
  {
    allFile(filter: { name: { eq: "print" } }) {
      nodes {
        publicURL
      }
    }
  }
`;

const Layout = ({ customClass, children }) => {
  const printCss = useStaticQuery(getPrintCss);
  const printCssUrl = printCss.allFile.nodes[0].publicURL;

  return (
    <>
      <Helmet>
        <link
          rel="stylesheet"
          type="text/css"
          href={printCssUrl}
          media="print"
        />
      </Helmet>
      <div className="main">
        <Navbar />
        <div className={`body ${customClass}`}>{children}</div>
        <Footer />
      </div>
    </>
  );
};

export default Layout;
