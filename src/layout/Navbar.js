import React, { useState, useContext } from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import { StaticImage } from 'gatsby-plugin-image';

import { Link, I18nContext } from '../../plugins/publicis-gatsby-plugin-i18n';

import Languages from '../components/Languages/Languages';
import Menu from '../components/Menu';

const menuPrimary = graphql`
  query menuPrimary {
    menuPrimaryEN: contentfulMenu(
      name: { eq: "Primary" }
      node_locale: { eq: "en" }
    ) {
      links {
        __typename
        ... on Node {
          ... on ContentfulLink {
            url
            targetBlank
            menuText: text
          }
          ... on ContentfulLandingPage {
            title
            slug
          }
          ... on ContentfulComposePage {
            title
            slug
          }
        }
      }
    }
    menuPrimaryFR: contentfulMenu(
      name: { eq: "Primary" }
      node_locale: { eq: "fr" }
    ) {
      links {
        __typename
        ... on Node {
          ... on ContentfulLink {
            url
            targetBlank
            text
          }
          ... on ContentfulLandingPage {
            title
            slug
          }
          ... on ContentfulComposePage {
            title
            slug
          }
        }
      }
    }
  }
`;

const Navbar = () => {
  const [showNav, setShowNav] = useState(false);

  const dataMenuPrimary = useStaticQuery(menuPrimary);
  const i18n = useContext(I18nContext);

  let data;
  if (i18n.language === 'en') {
    data = dataMenuPrimary.menuPrimaryEN.links;
  } else if (i18n.language === 'fr') {
    data = dataMenuPrimary.menuPrimaryFR.links;
  }

  console.log(data);

  return (
    <>
      <nav className="navbar is-transparent">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item">
            <StaticImage
              src="../assets/images/logo-digilab.svg"
              alt="Logo Digilab"
              layout="constrained"
              height={100}
            />
          </Link>
          <div
            className={showNav ? 'navbar-burger is-active' : 'navbar-burger'}
            data-target="navbarExampleTransparentExample"
            onClick={e => setShowNav(!showNav)}
          >
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>

        <div
          id="navbarExampleTransparentExample"
          className={showNav ? 'navbar-menu is-active' : 'navbar-menu'}
        >
          <div className="navbar-start">
            <Link
              to="/"
              className="navbar-item"
              activeClassName="navbar-item--active"
            >
              Home
            </Link>
            <Menu menuItems={data} />
          </div>

          <div className="navbar-end">
            <Languages />
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
