import React from 'react';

const Footer = () => {
  return (
    <footer className="footer">
      <div className="content has-text-centered">
        <p>
          &copy; {new Date().getFullYear()}{' '}
          <strong className="is-uppercase">Digilab</strong>
        </p>
      </div>
    </footer>
  );
};

export default Footer;
