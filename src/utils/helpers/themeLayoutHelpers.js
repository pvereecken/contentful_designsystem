export const initThemeLayout = value => {
  let classLayout;

  switch (value) {
    case 'Boxed':
      classLayout = 'container';
      break;

    default:
    case 'Fullwidth':
      classLayout = false;
      break;
  }

  return classLayout;
};
