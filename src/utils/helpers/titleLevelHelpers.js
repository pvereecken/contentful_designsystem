export const initTitleLevel = (
  value,
  content,
  layout,
  align,
  background,
  color
) => {
  var classNames = require('classnames');

  let classTitle = classNames('title', layout, align, background, color);

  switch (value) {
    case 'h1':
      return `<h1 class="${classTitle} is-1">${content}</h1>`;

    case 'h2':
      return `<h2 class="${classTitle} is-2">${content}</h2>`;

    case 'h3':
      return `<h3 class="${classTitle} is-3">${content}</h3>`;

    case 'h4':
      return `<h4 class="${classTitle} is-4">${content}</h4>`;

    case 'h5':
      return `<h5 class="${classTitle} is-5">${content}</h5>`;

    default:
    case 'Do not display':
      return false;
  }
};
