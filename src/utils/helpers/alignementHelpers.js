export const initVerticalAlign = value => {
  let classVerticalAlign;

  switch (value) {
    case 'Top':
      classVerticalAlign = 'is-align-items-flex-start';
      break;

    case 'Bottom':
      classVerticalAlign = 'is-align-items-flex-end';
      break;

    case 'Center':
      classVerticalAlign = 'is-align-items-center';
      break;

    default:
    case 'Stretch':
      classVerticalAlign = 'is-align-content-stretch';
      break;
  }

  return classVerticalAlign;
};

export const initHorizontalAlign = value => {
  let classHorizontalAlign;

  switch (value) {
    case 'Center':
      classHorizontalAlign = 'has-text-centered';
      break;

    case 'Justify':
      classHorizontalAlign = 'has-text-justified';
      break;

    case 'Right':
      classHorizontalAlign = 'has-text-right';
      break;

    case 'Space Between':
      classHorizontalAlign = 'is-justify-content-space-between';
      break;

    case 'Space Around':
      classHorizontalAlign = 'is-justify-content-space-around';
      break;

    default:
    case 'Left':
      classHorizontalAlign = 'has-text-left';
      break;
  }

  return classHorizontalAlign;
};
