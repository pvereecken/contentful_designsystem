export const initColumnsSize = value => {
  let classSize;

  switch (value) {
    case 'Three quarters':
      classSize = 'is-three-quarters';
      break;

    case 'Two thirds':
      classSize = 'is-two-thirds';
      break;

    case 'half':
    case 'img-2':
      classSize = 'is-half';
      break;

    case 'One third':
    case 'img-3':
      classSize = 'is-one-third';
      break;

    case 'One quarter':
    case 'img-4':
      classSize = 'is-one-quarter';
      break;

    case 'Four fifths':
      classSize = 'is-four-fifths';
      break;

    case 'Three fifths':
      classSize = 'is-three-fifths';
      break;

    case 'Two fifths':
      classSize = 'is-two-fifths';
      break;

    case 'One fifth':
    case 'img-5':
      classSize = 'is-one-fifth';
      break;

    case 'Auto':
    case 'img-1':
    default:
      classSize = false;
      break;
  }

  return classSize;
};

export const initSize = value => {
  let classSize;

  switch (value) {
    case 'Small':
      classSize = 'is-small';
      break;

    case 'Medium':
      classSize = 'is-medium';
      break;

    case 'Large':
      classSize = 'is-large';
      break;

    case 'Normal':
    default:
      classSize = false;
      break;
  }

  return classSize;
};
