export const initBkgColor = value => {
  let classBkgColor;

  switch (value) {
    case 'Primary':
      classBkgColor = 'has-background-primary';
      break;

    case 'Link':
      classBkgColor = 'has-background-link';
      break;

    case 'Info':
      classBkgColor = 'has-background-info';
      break;

    case 'Success':
      classBkgColor = 'has-background-success';
      break;

    case 'Warning':
      classBkgColor = 'has-background-warning';
      break;

    case 'Danger':
      classBkgColor = 'has-background-danger';
      break;

    case 'White':
      classBkgColor = 'has-background-white';
      break;

    case 'Black':
      classBkgColor = 'has-background-black';
      break;

    case 'Light':
      classBkgColor = 'has-background-light';
      break;

    case 'Dark':
      classBkgColor = 'has-background-dark';
      break;

    default:
      classBkgColor = false;
      break;
  }

  return classBkgColor;
};

export const initColor = value => {
  let classBkgColor;

  switch (value) {
    case 'Primary':
      classBkgColor = 'has-text-primary';
      break;

    case 'Link':
      classBkgColor = 'has-text-link';
      break;

    case 'Info':
      classBkgColor = 'has-text-info';
      break;

    case 'Success':
      classBkgColor = 'has-text-success';
      break;

    case 'Warning':
      classBkgColor = 'has-text-warning';
      break;

    case 'Danger':
      classBkgColor = 'has-text-danger';
      break;

    case 'White':
      classBkgColor = 'has-text-white';
      break;

    case 'Black':
      classBkgColor = 'has-text-black';
      break;

    case 'Light':
      classBkgColor = 'has-text-light';
      break;

    case 'Dark':
      classBkgColor = 'has-text-dark';
      break;

    default:
      classBkgColor = false;
      break;
  }

  return classBkgColor;
};
