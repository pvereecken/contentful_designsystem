import * as React from 'react';
import { useIntl } from '../../plugins/publicis-gatsby-plugin-i18n';

import Layout from '../layout/Layout';

const NotFoundPage = () => {
  const intl = useIntl();

  return (
    <Layout>
      <h1>{intl.formatMessage({ id: 'error.title' })}</h1>
      <p>{intl.formatMessage({ id: 'error.text' })}</p>
    </Layout>
  );
};

export default NotFoundPage;
