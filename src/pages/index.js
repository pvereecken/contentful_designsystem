import React from 'react';
//import { graphql } from 'gatsby';

import Layout from '../layout/Layout';
import SEO from '../components/SEO';

const Index = ({ pageContext }) => {
  /*const {
    textIntroduction,
    speakAboutUs,
  } = data.contentfulHomepage;*/

  const locale = pageContext.language;

  return (
    <Layout customClass="homepage">
      <SEO lang={locale} />

      <div className="container">
        <p>Page d'accueil</p>
      </div>

      {/*textIntroduction && (
        <section
          className="text__introduction"
          dangerouslySetInnerHTML={{
            __html: textIntroduction.childMarkdownRemark.html,
          }}
        />
        )*/}
    </Layout>
  );
};

/*export const query = graphql`
  query homepageQuery($language: String!) {
    contentfulHomepage(
      node_locale: { eq: $language }
      title: { eq: "Homepage" }
    ) {
      textIntroduction {
        childMarkdownRemark {
          html
        }
      }
    }
  }
`;*/

export default Index;
