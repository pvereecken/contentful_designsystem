# Initialisation du projet

## Gatsby

Pour initialiser le projet exécuter la commande :

`npx gatsby new [name]`

Cette commande crée un projet starter REACT Gatsby avec les plugins de base suivant :

- [gatsby-plugin-react-helmet](https://www.gatsbyjs.com/plugins/gatsby-plugin-react-helmet/)

- [gatsby-plugin-image](https://www.gatsbyjs.com/plugins/gatsby-plugin-image/)

- [gatsby-transformer-sharp](https://www.gatsbyjs.com/plugins/gatsby-transformer-sharp/)

- [gatsby-plugin-sharp](https://www.gatsbyjs.com/plugins/gatsby-plugin-sharp/)

- [gatsby-plugin-manifest](https://www.gatsbyjs.com/plugins/gatsby-plugin-manifest/)

### Plugins supplémentaires installés

- [gatsby-plugin-sass](https://www.gatsbyjs.com/plugins/gatsby-plugin-sass/)

- [gatsby-source-contentful](https://www.gatsbyjs.com/plugins/gatsby-source-contentful/) : configuration du plugin dans "gatsby-config.js" :
```js
{
  resolve:  'gatsby-source-contentful',
  options: {
    accessToken:  process.env.CONTENTFUL_ACCESS_TOKEN,
    spaceId:  process.env.CONTENTFUL_SPACE_ID,
    host:  process.env.CONTENTFUL_HOST,
  },
},
```

- [gatsby-transformer-remark](https://www.gatsbyjs.com/plugins/gatsby-transformer-remark/)

- [gatsby-plugin-sitemap](https://www.gatsbyjs.com/plugins/gatsby-plugin-sitemap/)

- [gatsby-source-filesystem](https://www.gatsbyjs.com/plugins/gatsby-source-filesystem/) : on utilise ce plugin pour récupérer le fichier print.css principalement. Voilà la config ajoutée dans "_gatsby-config.js_" :
```js
{
  resolve: 'gatsby-source-filesystem',  
  options: {  
    name: 'styles',  
    path: './src/assets/styles/',  
    ignore: [`*.scss`],  
  },  
},
```

### Variables environnement

##### env.development

Affichera les contenus qui sont publiés et en draft

    CONTENTFUL_SPACE_ID=#RECUPERER SPACE_ID CONTENTFUL#
    CONTENTFUL_ACCESS_TOKEN=#RECUPERER API_KEY PREVIEW CONTENTFUL#
    CONTENTFUL_HOST=preview.contentful.com

##### .env.production

N'affichera que les contenus publiés

    CONTENTFUL_SPACE_ID=#RECUPERER SPACE_ID CONTENTFUL#
    CONTENTFUL_ACCESS_TOKEN=#RECUPERER API_KEY DELIVERY CONTENTFUL#
    CONTENTFUL_HOST=cdn.contentful.com

### Module de langue

Module custom **publicis-gatsby-plugin-i18n**

Le plugin est placé dans ./plugins/publicis-gatsby-plugin-i18n

Au niveau du gatsby-config.js, il faut ajouter :
```js
{
  resolve:  'publicis-gatsby-plugin-i18n',
  options: {
    path:  `${__dirname}/src/i18n`,
    languages: ['fr', 'en'],
    defaultLanguage:  'fr',
    redirect:  true,
    staticPages: ['/', '/404.html', '/404/', '/dev-404-page/'],
  },
},
```
_Remarques :_ ne pas oublier de changer les "_languages_" en fonction de la configuration de votre contentful (prendre les noms entre parenthèses au niveau des _Locales settings_, dans la colonne "_locale_"

Le components contenant le switcher de langues se trouvent dans le dossier : _src > components > Languages > Languages.js_ . Il est complétement customisable que ce soit au niveau de la structure ou de la mise en page, il faut cependant veille à bien garder les mêmes éléments "dynamiques".

Le plugin permet également de gérer des contenus statics en multilangue. Pour cela, il faut :

1.  Aller creer un json dans le dossier "_src > i18n_". Les noms des json correspond aux noms indiqués dans l'option "languages" au niveau du gatsby-config.js.
2.  Pour l'utiliser, il faut ensuite appeler le module "useInt"

        	import { Link, useIntl } from  '../../plugins/publicis-gatsby-plugin-i18n';

    récupérer l'information de la langue :

        const  intl = useIntl();

    Pour afficher le bon wording :

        {intl.formatMessage({ id:  'id.dans.json' })}

    Des exemples de cette utilisation sont disponibles dans les fichiers suivants :

- src > pages > index.js
- src > pages > 404.js
- src > components > contact > contact-modal.js

### node

Utiliser la version 14.x.x de node (dernière à ce jour utilisée : `14.17.6`)

Utiliser NVM (node version manager) pour gérer les différentes version de node sur son poste en fonction du besoin des projets.

Pour windows : https://github.com/coreybutler/nvm-windows/releases/tag/1.1.8

Pour MacOS : https://github.com/nvm-sh/nvm/releases

### Prettier

Installer l'extension `ext install esbenp.prettier-vscode` https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode sur visual studio code.

[Prettier](https://prettier.io/ 'https://prettier.io/') is an opinionated code formatter. It enforces a consistent style by parsing your code and re-printing it with its own rules that take the maximum line length into account, wrapping code when necessary.

# Lancer le projet

1.  Une fois les sources récupérées, ne pas oublier de faire un

        npm install

2.  Ajouter à la racine du projet les 2 fichiers d'environnement

        .env.development
        .env.production

3.  Pour lancer le site en local, lancer la commande :

        npm start

4.  Pour lancer un build (comme si le site était déployé en prod), faire les commandes suivantes :

        npm run build
        npm run serve

# Modules utilisés

- framework CSS : [Bulma](https://bulma.io/)
- pour les icons : [react-icons](https://react-icons.github.io/react-icons/)
- pour les carousels : [swiper](https://swiperjs.com/)

# Autres remarques

- Ne récupérer dans les requêtes graphQL que les élements utiles (tous ce qui est requêté sera stocké dans le json)
- Pour afficher du html récupéré de contentful, il faut utiliser [dangerouslySetInnerHTML](https://linguinecode.com/post/complete-guide-react-dangerouslysetinnerhtml) **/!\ Ne pas utiliser sur des balises "p"**
