require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
});

console.log(`Fichier env: .env.${process.env.NODE_ENV}`);

module.exports = {
  siteMetadata: {
    title: 'Digilab',
    titleTemplate: '%s · Digilab',
    description: 'Site Example using gatsby, react and contentful.',
    siteUrl: process.env.SITE_URL,
    twitterUsername: '@digilab',
    defaultLanguage: 'fr',
  },
  plugins: [
    {
      resolve: 'gatsby-source-contentful',
      options: {
        accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
        spaceId: process.env.CONTENTFUL_SPACE_ID,
        host: process.env.CONTENTFUL_HOST,
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-image',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sitemap',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        icon: 'src/assets/images/icon.png',
      },
    },
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        // Footnotes mode (default: true)
        footnotes: true,
        // GitHub Flavored Markdown mode (default: true)
        gfm: true,
        // Plugins configs
        plugins: [],
      },
    },
    {
      resolve: 'publicis-gatsby-plugin-i18n',
      options: {
        path: `${__dirname}/src/i18n`,
        languages: ['fr', 'en'],
        defaultLanguage: 'fr',
        redirect: true,
        staticPages: ['/', '/404.html', '/404/', '/dev-404-page/'],
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'styles',
        path: './src/assets/styles/',
        ignore: [`*.scss`],
      },
    },
  ],
};
