const path = require('path');

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  // Templates
  const pageTemplate = path.resolve('./src/templates/page-template.js');
  const composePageTemplate = path.resolve(
    './src/templates/composepage-template.js'
  );
  const peopleTemplate = path.resolve('./src/templates/people-template.js');

  const result = await graphql(`
    query GetPages {
      allContentfulLandingPage {
        edges {
          node {
            node_locale
            contentful_id
            title
            slug
          }
        }
      }
      allContentfulComposePage {
        edges {
          node {
            node_locale
            contentful_id
            title
            slug
          }
        }
      }
    }
  `);

  // Create Page
  result.data.allContentfulLandingPage.edges.forEach(({ node }) => {
    const locale = node.node_locale;
    const slug = node.slug;
    const paths = [];

    result.data.allContentfulLandingPage.edges
      .filter(({ node: x }) => x.contentful_id === node.contentful_id)
      .forEach(({ node: x }) =>
        paths.push({ label: x.node_locale, link: `/${x.slug}` })
      );

    if (slug) {
      createPage({
        path: `${locale}/${slug}`,
        component: pageTemplate,
        context: {
          language: locale,
          slug: slug,
          i18n: {
            paths,
          },
        },
      });
    }
  });

  // Create Page
  result.data.allContentfulComposePage.edges.forEach(({ node }) => {
    const locale = node.node_locale;
    const slug = node.slug;
    const paths = [];

    result.data.allContentfulComposePage.edges
      .filter(({ node: x }) => x.contentful_id === node.contentful_id)
      .forEach(({ node: x }) =>
        paths.push({ label: x.node_locale, link: `/${x.slug}` })
      );

    if (slug) {
      createPage({
        path: `${locale}/${slug}`,
        component: composePageTemplate,
        context: {
          language: locale,
          slug: slug,
          i18n: {
            paths,
          },
        },
      });
    }
  });
};

exports.createSchemaCustomization = ({ actions, schema }) => {
  const { createTypes } = actions;
  const typeDefs = `
    type ContentfulHeroBanner {
      heroTitle: String
      heroSubtitle: String
      textColor: String
      backgroundColor: String
      heroSize: String
      horizontalAlignement: String
      parallaxBackgroundImage: Boolean
      link: String
      linkText: String
      linkType: String
      linkColor: String
      linkSize: String
    }

    type ContentfulGallery{
      textColor: String
      backgroundColor: String
    }

    type ContentfulLink {
      linkType: String
      linkColor: String
      linkSize: String
    }

    type ContentfulLayout {
      backgroundColor: String
    }

    type ContentfulLayoutColumn {
      backgroundColor: String
    }

    type ContentfulSpace {
      backgroundColor: String
    }

    type ContentfulTextContent{
      textColor: String
      backgroundColor: String
    }

    type ContentfulTitle{
      textColor: String
      backgroundColor: String
    }

    type ContentfulPictoTitleText{
      backgroundColor: String
      textColor: String
      pictoPosition: String
    }
  `;
  createTypes(typeDefs);
};
